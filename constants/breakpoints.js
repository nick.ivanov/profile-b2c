const lg = 1440;
const md = 1280;
const sm = 768;
const xs = 414;

const breakpoints = {
  lg,
  md,
  sm,
  xs,
};

export default breakpoints;
