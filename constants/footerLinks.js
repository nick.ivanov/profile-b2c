const footerLinks = [
  {
    name: 'Обращения',
    url: '/requests',
  },
  {
    name: 'Правовая информация',
    url: '/legal',
  },
];

export default footerLinks;
