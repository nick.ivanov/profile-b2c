import { types } from '../constants';

export function loadPhoto(value) {
  return {
    type: types.LOAD_USER_PHOTO,
    value,
  };
}

export function rememberState(newState) {
  return {
    type: types.REMEMBER_STATE,
    newState,
  };
}

export function getProfileSettings(newSettings) {
  return {
    type: types.GET_PROFILE_SETTINGS,
    newSettings,
  };
}
