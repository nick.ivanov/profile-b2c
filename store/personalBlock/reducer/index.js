import {
  personalInitials, types,
} from '../constants';

export default function personal(state = personalInitials, action) {
  const newState = { ...state };
  switch (action.type) {
    case types.GET_PROFILE_SETTINGS:
      newState.nickname.name = action.newSettings['profile:nickname'] || '';
      newState.owner = action.newSettings['profile:company'] || '';
      newState.fullname = action.newSettings['profile:name'] || '';
      newState.surname = action.newSettings['profile:lastname'];
      newState.name = action.newSettings['profile:firstname'];
      newState.lastname = action.newSettings['profile:patronym'];
      newState.birthdate = action.newSettings['profile:birthday'];
      newState.gender = action.newSettings['profile:gender'];
      newState.phone = action.newSettings['mobile:phone'];
      newState.email = action.newSettings['profile:email'];
      return newState;
    case types.REMEMBER_STATE:
      console.log(action);
      newState.nickname.name = action.newState.nickname.name;
      newState.inn.name = action.newState.inn.name;
      newState.snils.name = action.newState.snils.name;
      return newState;
    case types.LOAD_USER_PHOTO:
      newState.photo = action.value;
      return newState;
    default:
      return newState;
  }
}
