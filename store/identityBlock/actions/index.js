import { types } from '../constants';

export function addDocumentsPhoto(photo) {
  return {
    type: types.ADD_PHOTO,
    photo,
  };
}

export function addDocumentsPhotos(photoArray) {
  return {
    type: types.ADD_MULTIPLE_PHOTOS,
    photoArray,
  };
}

export function removeDocumentsPhoto(index) {
  return {
    type: types.REMOVE_PHOTO,
    index,
  };
}
