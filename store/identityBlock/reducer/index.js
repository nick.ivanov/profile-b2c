import {
  identityInitials, types,
} from '../constants';

export default function identity(state = identityInitials, action) {
  const newState = { ...state };
  switch (action.type) {
    case types.ADD_PHOTO:
      newState.photos.push(action.photo);
      return newState;
    case types.ADD_MULTIPLE_PHOTOS:
      Array.from(action.photoArray).forEach((photo) => {
        newState.photos.push(photo);
      });
      return newState;
    case types.REMOVE_PHOTO:
      newState.photos.splice(action.index, 1);
      return newState;
    default:
      return newState;
  }
}
