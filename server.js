const next = require('next');
const express = require('express');
const http = require('http');
const { createProxyMiddleware } = require('http-proxy-middleware');
const { parse } = require('url');

const proxyConfig = {
  '/api': {
    target: 'https://login.mts.ru',
    changeOrigin: true,
    secure: false,
  },
};

const app = next({ dev: true });
const handle = app.getRequestHandler();


app
  .prepare()
  .then(() => {
    const server = express();

    process.env.NODE_TLS_REJECT_UNAUTHORISED = '0';

    Object.keys(proxyConfig)
      .forEach((key) => {
        server.use(key, createProxyMiddleware(proxyConfig[key]));
      });

    server.get('*', (req, res) => handle(req, res));

    http.createServer(server)
      .listen(3000, '0.0.0.0', (err) => {
        if (err) {
          throw err;
        }
        console.log('> ready $$$ on http://localhost:3000');
      });
  });
