import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const UserEmailStyled = styled.span``;

const UserEmail = ({ email }) => (
  <UserEmailStyled>
    {email}
  </UserEmailStyled>
);

UserEmail.propTypes = {
  email: PropTypes.string,
};

UserEmail.defaultProps = {
  email: null,
};


export default UserEmail;
