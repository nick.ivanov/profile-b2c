import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';
import Icon from '../Icon';

const PhotoButtonStyled = styled.div`
  width: 2rem;
  height: 2rem;
  opacity: 0;
  border-radius: 50%;
  background: ${palette.photoButtonBack};
  position: absolute;
  bottom: .25rem;
  right: .25rem;
  transition: opacity .12s ease;
  cursor: pointer;
  z-index: 20;

  img {
    width: .875rem;
    height: auto;
    position: relative;
    cursor: pointer;
  }
`;

const FileInput = styled.input`
  opacity: 0;
  width: 0;
  height: 0;
  position: absolute;
  cursor: pointer;
  z-index: -1;

  & + label {
    width: 100%;
    height: 100%;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;


const PhotoButton = ({ changePhoto, register }) => (
  <PhotoButtonStyled className="photoButton">
    <FileInput type="file" id="file" ref={register} name="file" onChange={changePhoto} />
    {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
    <label htmlFor="file">
      <Icon name="changePhotoIcon" color={palette.white} />
    </label>
  </PhotoButtonStyled>
);

PhotoButton.propTypes = {
  changePhoto: PropTypes.func.isRequired,
  register: PropTypes.func,
};

PhotoButton.defaultProps = {
  register: null,
};

export default PhotoButton;
