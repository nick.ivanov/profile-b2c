import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';
import Icon from '../Icon';

const ProfileStatusStyled = styled.span`
  font-family: "MTSSans", sans-serif;
  font-size: .875rem;
  line-height: 1.25rem;
  display: inline-flex;
  align-items: center;
  text-align: center;
  color: ${palette.textBlack};
  padding: 0 .5rem;
  border: 1px solid ${palette.TextBlack};
  border-radius: .25rem;
`;

const QuestionButton = styled.button`
  border: 0;
  background: ${palette.gray};
  width: 1rem;
  height: 1rem;
  border-radius: 50%;
  margin-left: .5rem;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  outline: none;
`;

const ProfileStatus = ({ profileStatus }) => (
  <>
    <ProfileStatusStyled>
      {profileStatus}
    </ProfileStatusStyled>
    <QuestionButton type="button">
      <Icon name="questionIcon" color={palette.grayUltraLight} />
    </QuestionButton>
  </>
);

ProfileStatus.propTypes = {
  profileStatus: PropTypes.string,
};

ProfileStatus.defaultProps = {
  profileStatus: 'Не подтвержден',
};


export default ProfileStatus;
