import React from 'react';
import ProfileStatus from './index';

export default {
  component: ProfileStatus,
  title: 'Atoms|ProfileStatus',
  excludeStories: /.*data$/,
};

export const data = {
  profileStatus: 'Подтвержден',
};

export const Default = () => <ProfileStatus />;

export const Confirmed = () => <ProfileStatus profileStatus="Подтвержден" />;
