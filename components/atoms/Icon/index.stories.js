import React from 'react';
import Icon from './index';

export default {
  component: Icon,
  title: 'Atoms|Icon',
  excludeStories: /.*data$/,
};

export const icon = () => <Icon />;
