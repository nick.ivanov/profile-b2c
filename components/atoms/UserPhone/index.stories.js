import React from 'react';
import UserPhone from './index';

export default {
  component: UserPhone,
  title: 'Atoms|UserPhone',
  excludeStories: /.*data$/,
};

export const Default = () => <UserPhone phone="+7 994 434-13-24" isEmail={false} />;
