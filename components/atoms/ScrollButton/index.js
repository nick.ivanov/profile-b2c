import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Icon from '../Icon';
import palette from '../../palette';

const ScrollButtonStyled = styled.div`
  width: 1.5rem;
  height: 1.5rem;
  transform: rotateZ(180deg);
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  transition: transform .2s ease;
`;

const ScrollButton = ({ isOpened, onClick }) => (
  <ScrollButtonStyled style={isOpened ? { transform: 'rotateZ(0)' } : { transform: 'rotateZ(180deg)' }} onClick={onClick}>
    <Icon name="scrollButton" color={palette.grayRaven} />
  </ScrollButtonStyled>
);

ScrollButton.propTypes = {
  isOpened: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

ScrollButton.defaultProps = {
  isOpened: false,
};

export default ScrollButton;
