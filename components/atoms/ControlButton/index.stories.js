import React from 'react';
import { action } from '@storybook/addon-actions';
import ControlButton from './index';

export default {
  component: ControlButton,
  title: 'Atoms|ControlButton',
  excludeStories: /.*data$/,
};

export const Default = () => (
  <ControlButton icon={null} name="Редактировать" onClick={action('Changed')} id="1" />
);
