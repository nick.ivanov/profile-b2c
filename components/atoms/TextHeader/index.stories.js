import React from 'react';
import TextHeader from './index';

export default {
  component: TextHeader,
  title: 'Atoms|TextHeader',
  excludeStories: /.*data$/,
};

export const UserIdentity = () => <TextHeader header="Идентификация пользователя" />;
