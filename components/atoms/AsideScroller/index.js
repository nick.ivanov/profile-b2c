import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';

const AsideScrollerStyled = styled.div`
  height: 3.25rem;
  width: 2px; 
  background-color: ${palette.Red};
  position: absolute;
  top: 0;
  left: 0;
  transition: transform .2s ease;
`;

const AsideScroller = ({ position }) => <AsideScrollerStyled style={{ transform: `translateY(${position}px)` }} />;

AsideScroller.propTypes = {
  position: PropTypes.number,
};

AsideScroller.defaultProps = {
  position: 0,
};

export default AsideScroller;
