import React from 'react';
import CopyrightBlock from './index';

export default {
  component: CopyrightBlock,
  title: 'Atoms|CopyrightBlock',
  excludeStories: /.*data$/,
};

export const Default = () => <CopyrightBlock />;

export const NextYear = () => <CopyrightBlock year={2021} />;
