import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';

const CopyrightBlockStyled = styled.span`
  font-family: "MTSSans", sans-serif;
  font-size: 1rem;
  line-height: 1.5;
  font-feature-settings: 'tnum' on, 'lnum' on;
  color: ${palette.grayRaven};
`;

const CopyrightBlock = ({ year }) => (
  <CopyrightBlockStyled>
    {`© ${year} ПАО «МТС» 18+`}
  </CopyrightBlockStyled>
);

CopyrightBlock.propTypes = {
  year: PropTypes.number,
};

CopyrightBlock.defaultProps = {
  year: 2020,
};

export default CopyrightBlock;
