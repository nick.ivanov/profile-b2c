import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';

const UserNameStyled = styled.div`
  font-family: "MTSSans", sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 2rem;
  line-height: 1.25;
  letter-spacing: -.5px;
  font-feature-settings: 'tnum' on, 'lnum' on;
  color: ${palette.textBlack};
`;

const UserName = ({ name }) => <UserNameStyled>{name}</UserNameStyled>;

UserName.propTypes = {
  name: PropTypes.string.isRequired,
};

export default UserName;
