import React from 'react';
import UserName from './index';

export default {
  component: UserName,
  title: 'Atoms|UserName',
  excludeStories: /.*data$/,
};

export const Default = () => <UserName name="Христофор Петров" />;
