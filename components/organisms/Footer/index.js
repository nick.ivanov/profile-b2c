import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import CopyrightBlock from '../../atoms/CopyrightBlock';
import FooterLinks from '../../moleculas/FooterLinks';

const FooterStyled = styled.div`
  border-top: 1px solid rgba(0, 0, 0, .08);
  padding: 0 2rem;
  width: 100%;
  max-width: 75rem;
  height: 5.5rem;
  margin: auto auto 0;
  position: relative;
`;

const FooterContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
`;

const Footer = ({ year, links }) => (
  <FooterStyled>
    <FooterContainer>
      <CopyrightBlock year={year} />
      <FooterLinks links={links} />
    </FooterContainer>
  </FooterStyled>
);

export default Footer;

Footer.propTypes = {
  year: PropTypes.number,
  links: PropTypes.arrayOf(PropTypes.any).isRequired,
};

Footer.defaultProps = {
  year: 2020,
};
