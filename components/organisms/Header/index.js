import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import palette from '../../palette';
import HeaderLogo from '../../moleculas/HeaderLogo';
import TopWidgetImage from '../../moleculas/TopWidgetImage';
import { closeTopWidget, toggleTopWidget } from '../../../store/topWidget/actions';

const HeaderStyled = styled.div`
  background-color: ${palette.white};
  height: 4.25rem;
  box-shadow: 0 0 6px rgba(110, 119, 130, 0.14);
  width: 100%;
  position: fixed;
  z-index: 10000;
`;

const HeaderContainer = styled.div`
  width: 100%;
  max-width: 80rem;
  height: 100%;
  display: flex;
  margin: 0 auto;
  position: relative;
  box-sizing: border-box;
  padding: 0 3.5rem;
`;

const TopWidgetImageButton = styled.button`
  align-self: center;
  width: 2.5rem;
  height: 2.5rem;
  padding: 0;
  margin: 0 0 0 auto;
  border: none;
  background-color: transparent;
  cursor: pointer;
  outline: none;
`;


const Header = ({ personal, children }) => {
  const dispatch = useDispatch();
  const openWidget = () => {
    dispatch(toggleTopWidget());
  };

  useEffect(() => {
    document.addEventListener('click', (e) => {
      if (e.target.className.indexOf('TopWidget') === -1) {
        dispatch(closeTopWidget());
      }
    });
  }, []);

  return (
    <HeaderStyled>
      <HeaderContainer>
        <HeaderLogo />
        {children}
        <TopWidgetImageButton onClick={openWidget}>
          <TopWidgetImage
            name={personal.name}
            gender={personal.gender}
            image={personal.photo}
          />
        </TopWidgetImageButton>
      </HeaderContainer>
    </HeaderStyled>
  );
};

export default Header;

Header.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.objectOf(PropTypes.any),
    PropTypes.arrayOf(PropTypes.any)]).isRequired,
  personal: PropTypes.oneOfType([
    PropTypes.objectOf(PropTypes.any),
    PropTypes.arrayOf(PropTypes.any)]).isRequired,
};
