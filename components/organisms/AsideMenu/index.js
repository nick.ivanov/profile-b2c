import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import MenuItem from '../../moleculas/MenuItem';
import HorizontalSpacer from '../../atoms/HorisontalSpacer';
import palette from '../../palette';
import AsideScroller from '../../atoms/AsideScroller';

const AsideMenuStyled = styled.div`
  background: ${palette.white};
  display: flex;
  flex-direction: column;
  position: sticky;
  top: 2rem;
  width: 100%;
  max-width: 16.75rem;
  align-self: flex-start;
`;

const AsideMenuList = styled.ul`
  margin: 0;
  padding: 0;
`;

const AsideMenuContainer = styled.div`
  position: relative;
`;

const AsideMenu = ({ items, active, scrollToBlock }) => (
  <AsideMenuStyled>
    <HorizontalSpacer height="1rem" />
    <AsideMenuContainer>
      <AsideMenuList>
        {items.map((item, index) => (
          <MenuItem
            label={item.label}
            id={item.name}
            icon={item.icon}
            active={active === index}
            scrollTo={item.scrollTo}
            scrollToBlock={scrollToBlock}
            index={index}
            key={`MenuItem-${item.name}`}
          />
        ))}
      </AsideMenuList>
      <AsideScroller position={active * 52} />
    </AsideMenuContainer>
    <HorizontalSpacer height="1rem" />
  </AsideMenuStyled>
);

export default AsideMenu;


AsideMenu.propTypes = {
  items: PropTypes.arrayOf(PropTypes.any).isRequired,
  active: PropTypes.number,
  scrollToBlock: PropTypes.func.isRequired,
};

AsideMenu.defaultProps = {
  active: 0,
};
