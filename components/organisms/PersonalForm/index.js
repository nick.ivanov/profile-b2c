import React from 'react';
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import CustomField from '../../moleculas/CustomField';
import FieldsRow from '../../moleculas/FieldsRow';
import TextHeader from '../../atoms/TextHeader';
import BlockHeader from '../../moleculas/BlockHeader';
import FieldsBlock from '../../moleculas/FieldsBlock';
import FieldsCell from '../../moleculas/FieldsCell';
import HorisontalSpacer from '../../atoms/HorisontalSpacer';
// import { getProfileSettings } from '../../../store/personalBlock/actions';
import monthes from '../../../constants/monthes';

const PersonalForm = ({
  onSubmit, values, formID, rememberChanges,
}) => {
  const getDate = (date) => {
    const rawDate = new Date(date);
    const year = rawDate.getFullYear();
    const month = monthes[rawDate.getMonth()];
    const day = rawDate.getDate();
    return `${day} ${month} ${year}`;
  };

  const {
    register, getValues,
  } = useForm({
    defaultValues: {
      ...values,
      birthdate: getDate(values.birthdate),
    },
  });

  const getVals = () => {
    rememberChanges(getValues({ nest: true }));
  };


  return (
    <form onSubmit={onSubmit} id={formID} onChange={getVals}>
      <FieldsBlock>
        <BlockHeader>
          <TextHeader header="Личная информация" />
        </BlockHeader>
        <FieldsRow>
          <FieldsCell lg={12}>
            <CustomField
              id="nickname"
              name="nickname.name"
              label="Никнейм"
              register={register}
              value={values.nickname.name}
              buttons={values.nickname.buttons}
            />
          </FieldsCell>
        </FieldsRow>
        <HorisontalSpacer />
        {values.owner
        && (
          <>
            <FieldsRow>
              <FieldsCell lg={12}>
                <CustomField
                  id="owner"
                  name="owner"
                  label="Владелец номера"
                  register={register}
                  value={values.owner}
                />
              </FieldsCell>
            </FieldsRow>
            <HorisontalSpacer />
          </>
        )}
        <FieldsRow>
          <FieldsCell lg={3}>
            <CustomField
              id="surname"
              name="surname"
              label="Фамилия"
              register={register}
              value={values.surname}
            />
          </FieldsCell>
          <FieldsCell lg={3}>
            <CustomField
              id="name"
              name="name"
              label="Имя"
              register={register}
              value={values.name}
            />
          </FieldsCell>
          <FieldsCell lg={3}>
            <CustomField
              id="lastname"
              name="lastname"
              label="Отчество"
              register={register}
              value={values.lastname}
            />
          </FieldsCell>
        </FieldsRow>
        <HorisontalSpacer />
        <FieldsRow>
          <FieldsCell lg={3}>
            <CustomField
              id="birthdate"
              name="birthdate"
              label="Дата рождения"
              register={register}
              value={getDate(values.birthdate)}
            />
          </FieldsCell>
          {values.gender
          && (
            <FieldsCell lg={3}>
              <CustomField
                id="gender"
                name="gender"
                label="Пол"
                register={register}
                value={values.gender}
              />
            </FieldsCell>
          )}
        </FieldsRow>
        <HorisontalSpacer />
        <FieldsRow>
          <FieldsCell lg={3}>
            <CustomField
              id="inn"
              name="inn.name"
              label="ИНН"
              register={register}
              value={values.inn.name}
              buttons={values.inn.buttons}
            />
          </FieldsCell>
          <FieldsCell lg={6}>
            <CustomField
              id="snils"
              name="snils.name"
              label="СНИЛС"
              register={register}
              value={values.snils.name}
              buttons={values.snils.buttons}
            />
          </FieldsCell>
        </FieldsRow>
        <HorisontalSpacer />
      </FieldsBlock>
    </form>
  );
};

export default PersonalForm;

PersonalForm.propTypes = {
  values: PropTypes.objectOf(PropTypes.any).isRequired,
  onSubmit: PropTypes.func.isRequired,
  rememberChanges: PropTypes.func.isRequired,
  formID: PropTypes.string.isRequired,
};
