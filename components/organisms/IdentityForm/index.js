import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import FieldsRow from '../../moleculas/FieldsRow';
import FieldsCell from '../../moleculas/FieldsCell';
import TextHeader from '../../atoms/TextHeader';
import BlockHeader from '../../moleculas/BlockHeader';
import FieldsBlock from '../../moleculas/FieldsBlock';
import IdentityField from '../../moleculas/IdentityField';
import HorisontalSpacer from '../../atoms/HorisontalSpacer';
import palette from '../../palette';
import { removeDocumentsPhoto, addDocumentsPhotos } from '../../../store/identityBlock/actions';
// import identity from '../../../store/identityBlock/reducer';

function MyDropzone() {
  const dispatch = useDispatch();

  const convertToDataUrl = (file) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.onerror = reject;
    reader.readAsDataURL(file);
  });

  const uploadAllFiles = async (acceptedFiles) => {
    const filesUrlArray = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const file of acceptedFiles) {
      if (file.type.indexOf('image') !== -1) {
        try {
          // eslint-disable-next-line no-await-in-loop
          const convertedFile = await convertToDataUrl(file);
          filesUrlArray.push(convertedFile);
        } catch (err) {
          console.log(err);
        }
      }
    }
    return filesUrlArray;
  };

  const onDrop = useCallback(async (acceptedFiles) => {
    dispatch(addDocumentsPhotos(await uploadAllFiles(acceptedFiles)));
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <div {...getRootProps()}>
      <style jsx>
        {
        `
          span {
            display: block;
            height: 90px;
            border-radius: 4px;
            padding: 16px;
            box-sizing: border-box;
            border: 2px solid ${palette.blue};
          }
          
          .droppable {
            border-style: dashed;
          }
        `
      }
      </style>
      <input {...getInputProps()} />
      {
        isDragActive
          ? <span className="droppable">Отпускай кнопку!</span>
          : <span>Тащи сюда свои грязные фотки</span>
      }
    </div>
  );
}

const IdentityForm = ({ onSubmit, formID }) => {
  const goToGosuslugi = (e) => {
    e.preventDefault();
    window.location.href = 'https://www.gosuslugi.ru/';
  };

  const identities = useSelector((state) => (state.identity));
  const dispatch = useDispatch();

  const removeThisImage = (e) => {
    e.preventDefault();
    dispatch(removeDocumentsPhoto(e.target.dataset.index));
  };

  return (
    <form onSubmit={onSubmit} id={formID}>
      <FieldsBlock>
        <BlockHeader>
          <TextHeader header="Идентификация пользователя" />
        </BlockHeader>
        <FieldsRow>
          <FieldsCell lg={6}>
            <IdentityField
              image="gosuslugi"
              label="Заполнить с помощью госуслуг"
              id="gosuslugiButton"
              name="gosuslugiButton"
              legend="Данные подтянутся из вашего профиля"
              goToGosuslugi={goToGosuslugi}
            />
          </FieldsCell>
          <FieldsCell lg={6}>
            <IdentityField
              label="Скан паспорта"
              id="scanButton"
              name="scanButton"
              legend="Данные подтянутся после загрузки паспорта"
            />
          </FieldsCell>
        </FieldsRow>
        <HorisontalSpacer />
        <FieldsRow>
          <FieldsCell lg={6}>
            <MyDropzone />
          </FieldsCell>
        </FieldsRow>
        <HorisontalSpacer />
        <FieldsRow isWrap>
          {identities.photos.map((photo, index) => (
            <FieldsCell lg={2} key={photo}>
              <img src={photo} alt={`identity-${index}`} />
              <button type="button" data-index={index} onClick={removeThisImage}>Удалить фото</button>
            </FieldsCell>
          ))}
        </FieldsRow>
        <HorisontalSpacer />
      </FieldsBlock>
    </form>
  );
};

export default IdentityForm;

IdentityForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  formID: PropTypes.string.isRequired,
};
