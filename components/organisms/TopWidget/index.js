import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import TopWidgetItem from '../../moleculas/TopWidgetItem';
import TopWidgetUser from '../../moleculas/TopWidgetUser';
import palette from '../../palette';
import phoneReplacer from '../../../constants/phoneReplacer';

const TopWidgetStyled = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${palette.white};
  box-shadow: 0 0 4px rgba(128, 128, 128, 0.16), 0 5px 10px rgba(110, 119, 130, 0.16);
  border-radius: .25rem;
  padding: 0;
  width: 100%;
  max-width: 20rem;
  overflow: hidden;
  height: auto;
  margin-left: auto;
  position: absolute;
  right: 0;
  top: calc(100% + .25rem);
  max-height: 0; 
  justify-content: flex-start;
  outline: none;
  transition: max-height .2s ease, padding .2s linear;
  
  &.-active {
    max-height: 17rem;
    padding: .25rem 0;
  }
`;

const TopWidgetMenu = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
`;


const TopWidget = () => {
  const widget = useSelector((state) => (state.topWidget));
  const personal = useSelector((state) => (state.personal));
  return (
    <TopWidgetStyled
      className={widget.isShown ? '-active' : ''}
      id="topWidget"
      tabIndex="-1"
    >
      <TopWidgetUser
        phone={phoneReplacer(String(personal.phone))}
        name={`${personal.name} ${personal.surname}`}
        gender={personal.gender}
        owner={personal.owner}
        image={personal.photo}
      />
      <TopWidgetMenu>
        {widget.menuItems.map((item) => (
          // eslint-disable-next-line react/no-array-index-key
          <TopWidgetItem
            label={item.label}
            id={item.name}
            icon={item.icon}
            url={item.url}
            key={item.label + item.url}
          />
        ))}
      </TopWidgetMenu>
    </TopWidgetStyled>
  );
};

export default TopWidget;
