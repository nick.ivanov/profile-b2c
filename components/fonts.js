import { createGlobalStyle } from 'styled-components';

const GlobalFonts = createGlobalStyle`
@font-face {
  font-family: 'MTSSans';
  src: url('/mtssans/MTSSans-Regular__W.eot');
  src:
    url('/mtssans/MTSSans-Regular__W.eot?#iefix') format('embedded-opentype'),
    url('/mtssans/MTSSans-Regular__W.woff2') format('woff2'),
    url('/mtssans/MTSSans-Regular__W.woff') format('woff'),
    url('/mtssans/MTSSans-Regular__W.ttf') format('truetype'),
    url('/mtssans/MTSSans-Regular__W.svg#MTSSansWeb-Regular') format('svg');
  font-weight: 400;
  font-style: normal;
}

@font-face {
  font-family: 'MTSSans';
  src: url('/mtssans/MTSSans-Medium__W.eot');
  src:
    url('/mtssans/MTSSans-Medium__W.eot?#iefix') format('embedded-opentype'),
    url('/mtssans/MTSSans-Medium__W.woff2') format('woff2'),
    url('/mtssans/MTSSans-Medium__W.woff') format('woff'),
    url('/mtssans/MTSSans-Medium__W.ttf') format('truetype'),
    url('/mtssans/MTSSans-Medium__W.svg#MTSSansWeb-Medium') format('svg');
  font-weight: 500;
  font-style: normal;
}

@font-face {
  font-family: 'MTSSans';
  src: url('/mtssans/MTSSans-Bold__W.eot');
  src:
    url('/mtssans/MTSSans-Bold__W.eot?#iefix') format('embedded-opentype'),
    url('/mtssans/MTSSans-Bold__W.woff2') format('woff2'),
    url('/mtssans/MTSSans-Bold__W.woff') format('woff'),
    url('/mtssans/MTSSans-Bold__W.ttf') format('truetype'),
    url('/mtssans/MTSSans-Bold__W.svg#MTSSansWeb-Bold') format('svg');
  font-weight: 700;
  font-style: normal;
}
`;

export default GlobalFonts;
