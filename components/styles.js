import { createGlobalStyle } from 'styled-components';
import palette from './palette';

const gridArray = new Array(12);

const GlobalStyles = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    background-color: ${palette.bgGray};
  }
`;

export default GlobalStyles;
