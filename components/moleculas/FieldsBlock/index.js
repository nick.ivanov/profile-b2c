import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';

const FieldsBlockStyled = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${palette.white};
  font-family: "MTSSans", sans-serif;
`;

export default function FieldsBlock({ children }) {
  return (
    <FieldsBlockStyled>
      {children}
    </FieldsBlockStyled>
  );
}

FieldsBlock.propTypes = {
  children: PropTypes.arrayOf(PropTypes.any).isRequired,
};
