import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const FieldsToggleBlockStyled = styled.div`
  display: flex;
  flex-direction: column;
  height: auto;
  max-height: 0;
  overflow: hidden;
  transition: max-height .2s ease;
  
  &.-opened {
    max-height: 30rem;
  }
`;

export default function FieldsToggleBlock({ children, isOpened }) {
  return (
    <FieldsToggleBlockStyled className={isOpened ? '-opened' : ''}>
      {children}
    </FieldsToggleBlockStyled>
  );
}

FieldsToggleBlock.propTypes = {
  children: PropTypes.arrayOf(PropTypes.any).isRequired,
  isOpened: PropTypes.bool,
};

FieldsToggleBlock.defaultProps = {
  isOpened: true,
};
