import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import palette from '../../palette';
import HorizontalSpacer from '../../atoms/HorisontalSpacer';
import Icon from '../../atoms/Icon';


const IdentityField = ({
  image, label, id, name, component, legend, goToGosuslugi,
}) => {
  const IdentityFieldStyled = styled.button`
  height: 5.25rem;
  border: 2px solid ${palette.blue};
  border-radius: .25rem;
  box-sizing: border-box;
  display: flex;
  cursor: pointer;
  background-color: ${palette.white};
  text-align: left;
  padding: 1.25rem;
  width: 100%;
`;

  const TextPart = styled.div`
  display: flex;
  flex-direction: column;
  font-family: "MTSSans", sans-serif;
  
  span {
    color: ${palette.lightBlue};
    font-weight: 500;
    font-size: 1rem;
    line-height: 1.5;
  }
  
  legend {
    font-size: .75rem;
    line-height: 1rem;
    color: ${palette.grayRaven};
    mix-blend-mode: normal;
  }
`;

  const ImagePart = styled.div`
  height: 2.75rem;
  width: 2.75rem;
  margin-left: auto;
  display: flex;
  justify-content: center;
  align-items: center;
  
  img {
    max-width: 100%;
    height: auto;
  }
`;

  return (
    <IdentityFieldStyled id={id} name={name} component={component} onClick={goToGosuslugi}>
      <TextPart>
        <span>{label}</span>
        <HorizontalSpacer height=".25rem" />
        <legend>{legend}</legend>
      </TextPart>
      {image
        ? (
          <ImagePart>
            <Icon name={image} />
          </ImagePart>
        )
        : ''}
    </IdentityFieldStyled>
  );
}


export default IdentityField;


IdentityField.propTypes = {
  image: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.string.isRequired,
  legend: PropTypes.string,
  name: PropTypes.string.isRequired,
  component: PropTypes.string,
  goToGosuslugi: PropTypes.func,
};

IdentityField.defaultProps = {
  image: null,
  label: null,
  legend: null,
  component: 'button',
  goToGosuslugi: () => {},
};
