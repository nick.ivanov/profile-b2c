import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ScrollButton from '../../atoms/ScrollButton';

const BlockHeaderStyled = styled.div`
  height: 5.75rem;
  width: 100%;
  display: flex;
  padding: 0 2rem;
  align-items: center;
  justify-content: space-between;
  box-sizing: border-box;
`;

export default function BlockHeader({
  children, isScrollable, isOpened, toggleOpened,
}) {
  return (
    <BlockHeaderStyled>
      {children}
      {isScrollable ? (
        <ScrollButton
          isOpened={isOpened}
          onClick={toggleOpened}
        />
      ) : ''}
    </BlockHeaderStyled>
  );
}

BlockHeader.propTypes = {
  isScrollable: PropTypes.bool,
  children: PropTypes.objectOf(PropTypes.any).isRequired,
  isOpened: PropTypes.bool,
  toggleOpened: PropTypes.func,
};

BlockHeader.defaultProps = {
  isScrollable: false,
  isOpened: false,
  toggleOpened: null,
};
