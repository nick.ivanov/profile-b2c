import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import PhotoButton from '../../atoms/PhotoButton';

const UserPhotoStyled = styled.div`
  display: flex;
  width: 8.5rem;
  height: 8.5rem;
  position: relative;
  
  &:hover {
    .photoButton {
      opacity: 1;
    }
  }
`;

const UserImage = styled.img`
  border-radius: 50%;
  width: 100%;
  height: auto;
`;


const UserPhoto = ({
  image = '/images/defaultUserPhoto.svg', name, changePhoto, register,
}) => (
  <UserPhotoStyled>
    <UserImage name="photo" src={image} alt={name} />
    <PhotoButton changePhoto={changePhoto} register={register} />
  </UserPhotoStyled>
);

UserPhoto.propTypes = {
  image: PropTypes.string,
  name: PropTypes.string,
  changePhoto: PropTypes.func.isRequired,
  register: PropTypes.func.isRequired,
};

UserPhoto.defaultProps = {
  image: '/images/defaultUserPhoto.svg',
  name: 'User photo',
};

export default UserPhoto;
