import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import UserName from '../../atoms/UserName';
import UserPhone from '../../atoms/UserPhone';
import UserEmail from '../../atoms/UserEmail';
import ProfileStatus from '../../atoms/ProfileStatus';
import HorizontalSpacer from '../../atoms/HorisontalSpacer';
import palette from '../../palette';

const UserInfoStyled = styled.div`
  margin-left: 2rem;
  align-self: center;
`;

const UserContacts = styled.div`
  font-family: "MTSSans", sans-serif;
  font-size: 1.25rem;
  line-height: 1.4;
  font-feature-settings: 'tnum' on, 'lnum' on;
  color: ${palette.textBlack};
`;


const UserInfo = ({
  name, surname, phone, email, isProfileConfirmed,
}) => (
  <UserInfoStyled>
    <UserName name={(name || surname) ? `${name} ${surname}` : phone} />
    <HorizontalSpacer height=".25em" />
    <UserContacts>
      <UserPhone phone={name ? String(phone) : null} isEmail={!!email} />
      <UserEmail email={email} />
    </UserContacts>
    <HorizontalSpacer height=".5em" />
    <ProfileStatus isProfileConfirmed={isProfileConfirmed} />
  </UserInfoStyled>
);

UserInfo.propTypes = {
  name: PropTypes.string,
  surname: PropTypes.string,
  phone: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  email: PropTypes.string,
  isProfileConfirmed: PropTypes.bool,
};

UserInfo.defaultProps = {
  name: null,
  surname: null,
  email: null,
  isProfileConfirmed: false,
};

export default UserInfo;
