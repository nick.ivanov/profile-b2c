const webpack = require('webpack');

module.exports = async ({ config, mode }) => {
  config.module.rules[4] = {
    test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|cur|ani)(\?.*)?$/,
    loader: './node_modules/file-loader/dist/cjs.js',
    query: {
      name: '/media/[name].[ext]',
    },
  };

  config.module.rules.push({
    test: /\.svg$/,
    use: [
      {
        loader: 'svg-inline-loader?removeSVGTagAttrs=false',
      },
    ],
  });

  return config;
};
